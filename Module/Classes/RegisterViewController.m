//
//  RegisterViewController.m
//  Login
//
//  Created by 罗贤明 on 2018/5/12.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "RegisterViewController.h"
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "DemoGround.h"
#import "LoginUserInfoModel.h"
#import "API.h"

@interface RegisterViewController ()

@property (nonatomic,strong) UITextField *accountTextField;

@property (nonatomic,strong) UITextField *passowrdTextField;

@property (nonatomic,strong) UITextField *firstNameTextField;

@property (nonatomic,strong) UITextField *lastNameTextField;

@property (nonatomic,strong) UISegmentedControl *genderSelector;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"注册";
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    UIView *container = [[UIView alloc] init];
    [self.view addSubview:container];
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.centerY.equalTo(self.view);
        make.height.equalTo(320);
    }];
    
    UILabel *accountLabel = [[UILabel alloc] init];
    accountLabel.text = @"帐号";
    accountLabel.translatesAutoresizingMaskIntoConstraints = NO;
    accountLabel.textAlignment = NSTextAlignmentCenter;
    accountLabel.font = [UIFont systemFontOfSize:16];
    [container addSubview:accountLabel];
    [accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(container);
        make.left.equalTo(container).offset(20);
        make.height.equalTo(36);
        make.width.equalTo(50);
    }];
    
    UITextField *accountInput = [[UITextField alloc] init];
    accountInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    accountInput.translatesAutoresizingMaskIntoConstraints = NO;
    accountInput.font = [UIFont systemFontOfSize:14];
    accountInput.layer.cornerRadius = 3;
    accountInput.layer.masksToBounds = YES;
    accountInput.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.3].CGColor;
    accountInput.layer.borderWidth = 0.5;
    [container addSubview:accountInput];
    [accountInput mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(accountLabel);
        make.right.equalTo(container).offset(-10);
        make.width.equalTo(200);
    }];
    _accountTextField = accountInput;
    // 姓
    UILabel *lastNameLabel = [[UILabel alloc] init];
    lastNameLabel.text = @"姓";
    lastNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    lastNameLabel.textAlignment = NSTextAlignmentCenter;
    lastNameLabel.font = [UIFont systemFontOfSize:16];
    [container addSubview:lastNameLabel];
    [lastNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountLabel.mas_bottom).offset(12);
        make.left.equalTo(container).offset(20);
        make.height.equalTo(36);
        make.width.equalTo(50);
    }];
    
    UITextField *lastNameInput = [[UITextField alloc] init];
    lastNameInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    lastNameInput.translatesAutoresizingMaskIntoConstraints = NO;
    lastNameInput.font = [UIFont systemFontOfSize:14];
    lastNameInput.layer.cornerRadius = 3;
    lastNameInput.layer.masksToBounds = YES;
    lastNameInput.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.3].CGColor;
    lastNameInput.layer.borderWidth = 0.5;
    [container addSubview:lastNameInput];
    [lastNameInput mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(lastNameLabel);
        make.right.equalTo(container).offset(-10);
        make.width.equalTo(200);
    }];
    _lastNameTextField = lastNameInput;
    
    // 名
    UILabel *firstNameLabel = [[UILabel alloc] init];
    firstNameLabel.text = @"名";
    firstNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    firstNameLabel.textAlignment = NSTextAlignmentCenter;
    firstNameLabel.font = [UIFont systemFontOfSize:16];
    [container addSubview:firstNameLabel];
    [firstNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lastNameLabel.mas_bottom).offset(12);
        make.left.equalTo(container).offset(20);
        make.height.equalTo(36);
        make.width.equalTo(50);
    }];
    
    UITextField *firstNameInput = [[UITextField alloc] init];
    firstNameInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    firstNameInput.translatesAutoresizingMaskIntoConstraints = NO;
    firstNameInput.font = [UIFont systemFontOfSize:14];
    firstNameInput.layer.cornerRadius = 3;
    firstNameInput.layer.masksToBounds = YES;
    firstNameInput.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.3].CGColor;
    firstNameInput.layer.borderWidth = 0.5;
    [container addSubview:firstNameInput];
    [firstNameInput mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(firstNameLabel);
        make.right.equalTo(container).offset(-10);
        make.width.equalTo(200);
    }];
    _firstNameTextField = firstNameInput;
    
    // 性别
    UILabel *genderLabel = [[UILabel alloc] init];
    genderLabel.text = @"性别";
    genderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    genderLabel.textAlignment = NSTextAlignmentCenter;
    genderLabel.font = [UIFont systemFontOfSize:16];
    [container addSubview:genderLabel];
    [genderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(firstNameInput.mas_bottom).offset(12);
        make.left.equalTo(container).offset(20);
        make.height.equalTo(36);
        make.width.equalTo(50);
    }];
    
    UISegmentedControl *genderSelector = [[UISegmentedControl alloc] initWithItems:@[@"男",@"中",@"女"]];
    genderSelector.selectedSegmentIndex = 1;
    [container addSubview:genderSelector];
    [genderSelector mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(genderLabel);
        make.right.equalTo(container).offset(-10);
        make.width.equalTo(200);
    }];
    _genderSelector = genderSelector;
    
    UILabel *passowrdLabel = [[UILabel alloc] init];
    passowrdLabel.text = @"密码";
    passowrdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    passowrdLabel.textAlignment = NSTextAlignmentCenter;
    passowrdLabel.font = [UIFont systemFontOfSize:16];
    [container addSubview:passowrdLabel];
    [passowrdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(genderLabel.mas_bottom).offset(12);
        make.left.equalTo(container).offset(20);
        make.height.equalTo(36);
        make.width.equalTo(50);
    }];
    
    UITextField *passowrdInput = [[UITextField alloc] init];
    passowrdInput.translatesAutoresizingMaskIntoConstraints = NO;
    passowrdInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    passowrdInput.font = [UIFont systemFontOfSize:14];
    passowrdInput.secureTextEntry = YES;
    passowrdInput.layer.cornerRadius = 3;
    passowrdInput.layer.masksToBounds = YES;
    passowrdInput.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.3].CGColor;
    passowrdInput.layer.borderWidth = 0.5;
    [container addSubview:passowrdInput];
    [passowrdInput mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(passowrdLabel);
        make.right.equalTo(container).offset(-10);
        make.width.equalTo(200);
    }];
    _passowrdTextField = passowrdInput;
    
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    registerButton.translatesAutoresizingMaskIntoConstraints = NO;
    [registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6] forState:UIControlStateHighlighted];
    [registerButton setBackgroundColor:[UIColor colorWithRed:72./255 green:143./255 blue:240./255 alpha:1]];
    registerButton.layer.masksToBounds = YES;
    registerButton.layer.cornerRadius = 4;
    registerButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    registerButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [registerButton addTarget:self action:@selector(register) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:registerButton];
    [registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passowrdInput.mas_bottom).offset(35);
        make.centerX.equalTo(container);
        make.width.equalTo(120);
        make.height.equalTo(44);
    }];
    
}

- (void)register {
    NSString *account = _accountTextField.text;
    if ([account isEqualToString:@""]) {
        [self toastMessage:@"请输入帐号！！"];
        return;
    }
    NSString *passowrd = _passowrdTextField.text;
    if ([passowrd isEqualToString:@""]) {
        [self toastMessage:@"请输入密码！！"];
        return;
    }
    NSString *lastName = _lastNameTextField.text;
    if ([lastName isEqualToString:@""]) {
        [self toastMessage:@"请输入姓！！"];
        return;
    }
    NSString *firstName = _firstNameTextField.text;
    if ([firstName isEqualToString:@""]) {
        [self toastMessage:@"请输入名！！"];
        return;
    }
    NSString *gender = [_genderSelector titleForSegmentAtIndex:_genderSelector.selectedSegmentIndex];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = @"注册中。。。";
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        hud.mode = MBProgressHUDModeText;
        hud.label.text = @"注册成功!!!";
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            AXEData *data = [AXEData dataForTransmission];
            LoginUserInfoModel *userInfo = [[LoginUserInfoModel alloc] init];
            userInfo.account = account;
            userInfo.level = @1;
            userInfo.detailInfo = @{
                                    @"firstName": firstName,
                                    @"lastName": lastName,
                                    @"gender": gender
                                    };
            userInfo.tagList = @[];
            
            [data setData:userInfo forKey:LOGIN_DATA_USERINFO];
            [[AXEData sharedData] setData:userInfo forKey:LOGIN_DATA_USERINFO];
            [data setBool:YES forKey:LOGIN_DATA_LOGIN];
            // TODO data需要copy一份。。。
            [AXEEvent postEventName:LOGIN_EVENT_LOGINSTATUS withPayload:data];
            [AXEEvent postEventName:LOGIN_EVENT_REGISTERSUCCESS withPayload:data];
            if (self.routeRequest.callback) {
                // 如果有回调。
                self.routeRequest.callback(data);
            }
            // 关闭页面。
            [self.navigationController popToViewController:self.routeRequest.fromVC animated:YES];
        });
    });
}

- (void)toastMessage:(NSString *)massage {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabel.text = massage;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
}

- (void)dismissKeyboard {
    [_passowrdTextField resignFirstResponder];
    [_accountTextField resignFirstResponder];
    [_firstNameTextField resignFirstResponder];
    [_lastNameTextField resignFirstResponder];
}


@end
