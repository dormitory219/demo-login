//
//  RegisterViewController.h
//  Login
//
//  Created by 罗贤明 on 2018/5/12.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AXEViewController.h"

/**
  注册界面
 */
@interface RegisterViewController : AXEViewController

@end
